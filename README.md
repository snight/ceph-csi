# Ceph CSI

Ceph CSI 插件实现了启用 CSI 的 Container Orchestrator（CO）和 Ceph 集群之间的接口。它允许动态配置 Ceph 卷并将其挂载到工作负载上，并通过不同的插件来支持 RBD 和 CephFS 这两个不同的存储接口。

# 一、准备环境
- Kubernetes集群版本在1.18及以上，集群必须允许特权Pod（`privileged pods`），即apiserver和kubelet需要设置`--allow-privileged`为`true`
- Ceph分布式存储集群版本在15及以上，使用`cephadmin`工具进行部署，已经创建一个名称为`ceph-block`的RBD类型的存储池。

## Ceph-CSI 涉及镜像
- quay.io/k8scsi/csi-provisioner:v2.0.4
- quay.io/k8scsi/csi-snapshotter:v3.0.2
- quay.io/k8scsi/csi-attacher:v3.0.2
- quay.io/k8scsi/csi-resizer:v1.0.1
- quay.io/cephcsi/cephcsi:canary
- k8s.gcr.io/sig-storage/csi-node-driver-registrar:v2.0.1

为了给镜像下载加速，本项目中已经将yaml文件中所有的`image`地址更换为阿里云的。

## Ceph集群环境

主机名|IP地址|端口|组件
--|:--:|--:|--
k8s-node1|10.10.51.124|3300,6789|ceph-mgr,ceph-mon,ceph-osd
k8s-node2|10.10.51.125|3300,6789|ceph-mgr,ceph-mon,ceph-osd
k8s-node3|10.10.51.126|3300,6789|ceph-mgr,ceph-mon,ceph-osd
k8s-node4|10.10.51.127|3300,6789|ceph-osd
k8s-node5|10.10.51.128|3300,6789|ceph-osd
k8s-node6|10.10.51.129|3300,6789|ceph-osd

# 二、安装ceph-csi插件
## 1、下载插件仓库
```
git clone https://gitlab.com/snight/ceph-csi.git
```
## 2、安装部署到K8S
```bash
cd ceph-csi
kubectl create -f csi-provisioner-rbac.yaml
kubectl create -f csi-nodeplugin-rbac.yaml
kubectl create -f csi-rbdplugin-provisioner.yaml
kubectl create -f csi-rbdplugin.yaml
```
# 三、配置Ceph集群
## 1、创建RBD类型的存储池并授权
登录到ceph-mgr角色的主机
```
创建一个ceph pool，命名为ceph-rbd
$ ceph osd pool create ceph-rbd
$ rbd pool init ceph-rbd
```
创建ceph用户
```shell
$ ceph auth get-or-create client.admin mon 'allow *' osd 'allow *' mgr 'allow *'
[client.admin]
  key = AQDCFstfFVwhOxAAUZMrW/K2wytnXzh2GFps6w==
```
修改权限
```shell
$ ceph auth caps client.admin mon 'allow rw pool=ceph-rbd' osd 'allow rw pool=ceph-rbd'
```
## 2、生成Ceph-CSI的ConfigMap
将ceph的监视地址以configmap形式存储在kubernetes中，作为ceph集群的一个对象，使用下面命令收集ceph的fsid和地址
```bash
$ ceph mon dump
dumped monmap epoch 13
epoch 13
fsid 7b731fac-36b8-11eb-a544-005056a7ff0a
last_changed 2020-12-05T07:39:02.520488+0000
created 2020-12-05T05:12:35.659052+0000
min_mon_release 15 (octopus)
0: [v2:10.10.51.124:3300/0,v1:10.10.51.124:6789/0] mon.k8s-node1
1: [v2:10.10.51.126:3300/0,v1:10.10.51.126:6789/0] mon.k8s-node3
2: [v2:10.10.51.125:3300/0,v1:10.10.51.125:6789/0] mon.k8s-node2
```
创建csi-config-map.yaml文件，将上面的 fsid 的字符串复制到 clusterID 字段的后面，将ceph-mon角色的地址替换到 monitors 的后面：
```yaml
cat > ceph-csi-config << EOF
apiVersion: v1
kind: ConfigMap
data:
  config.json: |-
    [
      {
        "clusterID": "7b731fac-36b8-11eb-a544-005056a7ff0a",
        "monitors": [
          "10.10.51.124:6789",
          "10.10.51.125:6789",
          "10.10.51.126:6789"
        ]
      }
    ]
metadata:
  name: ceph-csi-config
EOF
```
创建configmap资源清单
```
kubectl create -f ceph-csi-config.yaml
```
## 3、生成Ceph-CSI的CEPHX密钥
ceph-csi需要使用cephx凭据才能与Ceph集群进行通信。使用新创建的用户ID和cephx密钥生成下面的ceph-csi-secret.yaml文件：
```yaml
cat > ceph-csi-secret.yaml << EOF
apiVersion: v1
kind: Secret
metadata:
  name: ceph-csi-secret
  namespace: default
stringData:
  userID: admin
  userKey: AQDCFstfFVwhOxAAUZMrW/K2wytnXzh2GFps6w==
EOF
```
创建secret资源清单
```
kubectl apply -f ceph-csi-secret.yaml
```
# 三、集成Ceph和Kubernetes
## 1、创建存储类
- 存储类名为ceph-block
- clusterID为ceph集群fsid的字符串
- secret名称为ceph-csi-secret
- 默认空间为default
- ceph pool 为ceph-rbd
- reclaimPolicy设置为Delete，一旦删除POD资源，持久化数据也会丢失。
```yaml
cat > csi-rbd-sc.yaml  << EOF
---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: ceph-block
provisioner: rbd.csi.ceph.com
parameters:
  clusterID: 7b731fac-36b8-11eb-a544-005056a7ff0a
  pool: ceph-rbd
  csi.storage.k8s.io/provisioner-secret-name: ceph-csi-secret
  csi.storage.k8s.io/provisioner-secret-namespace: default
  csi.storage.k8s.io/node-stage-secret-name: ceph-csi-secret
  csi.storage.k8s.io/node-stage-secret-namespace: default
reclaimPolicy: Delete
mountOptions:
  - discard 
EOF
```
## 3、创建存储资源类
```
kubectl create -f ceph-storageclass.yaml
```
## 4、创建PV
storageClassName为上面创建的存储资源类名ceph-block
```yaml
cat > ceph-pvc-test.yaml  << EOF
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: ceph-pvc-test
spec:
  accessModes: 
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi 
  storageClassName: ceph-block
EOF
```
确认PVC状态为Bound表示正常
```shell
$ kubectl get sc
NAME         PROVISIONER        RECLAIMPOLICY   VOLUMEBINDINGMODE   ALLOWVOLUMEEXPANSION   AGE
ceph-block   rbd.csi.ceph.com   Delete          Immediate           false                  90m
$ kubectl get pvc
NAME            STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
ceph-pvc-test   Bound    pvc-0a9b0164-2da0-4c9e-a62d-b973accef960   1Gi        RWO            ceph-block     28s
```
## 5、删除PVC/PV
```shell
# 删除PVC
kubectl delete pvc ceph-pvc-test
# 获取pvid
kubectl get pv
# 更改PV的回收策略,更改为删除后保留持久数据
kubectl patch pv [pvid] -p '{"spec":{"persistentVolumeReclaimPolicy":"Retain"}}'
# 删除pv上的数据
kubectl delete pv [pvid]
```
